package vneuron.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;

@Controller
public class MainController {
  @ApiOperation(value = "swagger with REST api", response = Iterable.class)

  @RequestMapping("/")
  @ResponseBody
  public String index() {
    return "vneuron"+ " islem sawgger spring test api  " ;
  }

}
