package vneuron.controllers;

import vneuron.models.User;
import vneuron.models.UserDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


//https://app.gotomeeting.com/?meetingId=355549229
@Controller
@RequestMapping(value="/user")
@Api(value="vneuron")
public class UserController {

  @Autowired
  private UserDao _userDao;
  //http://localhost:8080//user/delete?id=2
  @ApiOperation(value = "delete user", response = Iterable.class)

  @RequestMapping(value="/delete", method= RequestMethod.DELETE)
  @ResponseBody
  public String delete(long id) {
    try {
      User user = new User(id);
      _userDao.delete(user);
    }
    catch(Exception ex) {
      return ex.getMessage();
    }
    return "User succesfully deleted!";
  }
  //http://localhost:8080//user/get-by-email?email=islemlouati@hotmail.com
  @ApiOperation(value = "get  user by  email ", response = Iterable.class)

  @RequestMapping(value="/get-by-email", method= RequestMethod.GET)
  @ResponseBody
  public String getByEmail(String email) {
    String userId;
    String userName;
    try {
      User user = _userDao.getByEmail(email);
      userId = String.valueOf(user.getId());
      userName = String.valueOf(user.getName());
    }
    catch(Exception ex) {
      return "User not found";
    }
    return "id user  is: " + userId 
    		+ "The user name  is "+ userName;
  }

//localhost:8080/userAccount/add?userName=Islem&password=123456&email=
  //* islemlouati@hotmail.com
  //http://localhost:8080//user/save?email=mohamedlouati@hotmail.com&name=mohamed
  @ApiOperation(value = "adding  user with his email and username", response = Iterable.class)
  
  @RequestMapping(value="/save", method= RequestMethod.POST)
  @ResponseBody
  public String create(String email, String name) {
    try {
      User user = new User(email, name);
      _userDao.save(user);
    }
    catch(Exception ex) {
      return ex.getMessage();
    }
    return "User saved !";
  }
  

 

} // class UserController
