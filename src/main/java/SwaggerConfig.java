import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {
	
	 @RequestMapping(value = "/docs", method = RequestMethod.GET)
	    public String docs(){
	        return "redirect:/swagger-ui.html";
	    }
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("vneuron-test-swagger-spring")
        		.apiInfo(metaData())
                .select()
               // .apis(RequestHandlerSelectors.basePackage("vneuron.controllers"))
                .apis(RequestHandlerSelectors.any())
               // .paths(regex("/user.*"))
                .paths(PathSelectors.any()) 
                .build();
                
    }
    private ApiInfo metaData() {
    	return new ApiInfo("Spring Boot REST API with Swagger",
    			"\"Spring Boot REST API with Swagger\"",
    			"1.0",
    			"Apache License Version 2.0",
    			new Contact("Islem Louati", "Vneuron", "islem.louati@vneuron.com"),
    			"https://www.apache.org/licenses/LICENSE-2.0\"",
    			""
    			);
        /*return new ApiInfoBuilder()
                .title("Spring Boot REST API with Swagger")
                .description("\"Spring Boot REST API with Swagger\"")
                .version("1.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"")
                .contact(new Contact("Islem Louati", "Vneuron", "islem.louati@vneuron.com"))
                .build();*/
    }
    
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}